# RS Toolkit

## Overview

This is a set of scripts to make your day-to-day workflow a bit more efficient. This toolkit
gives you the ability to easily run scripts in any language you prefer. For example templates, 
check out the `templates/` folder.

## Setup

The first order of business is to create a way to run this script easily within your shell. To get
started, run `ruby tools.rb setup:zsh`. This will give you a code snippet to place
in your `.zshrc` for tab completion. After adding this to your `~/.zshrc`, you can run `source ~/.zshrc`
to read the new changes. In addition to tab completion, this will enable you to 
use the toolkit with the `tools` command.

## Creating scripts

Creating a script is pretty straightforward. The thing to note here is that all scripts
are "name-spaced" by a parent folder. For example, `tools git:clone` would run the 
script found in `lib/git/` named `clone.*`. So, to create your script, you can...

1. Create a parent folder for your script
2. Create the script itself
3. Within the script, specify the interpreter (i.e. `#!/bin/bash`)
4. Save & Run
5. Additionally, you can create a file in the same folder named `scriptname.extension.txt` 
 to provide a readme in the case the user hits your script with the `--help` argument. 

## Some further notes on the `--help` argument

If a user calls your script with the `--help` argument, your script is bypassed and a 
help file named the same as your script appended with `.txt` will be displayed to the
user. For example, a script residing at `lib/git/clone.sh` can be invoked by running
`tools git:clone`. However, if you type `tools git:clone --help`, the program will attempt
to read a help file at `lib/git/clone.sh.txt`. 

## User scripts

The default directory for scripts is the `lib/` directory within the project root. However, 
there may be scripts that are specific to your workflow that shouldn't be a part of the 
core library. In that instance, You can create the `folder/script` structure within 
`~/.tools`. 

Anything in the `~/.tools` folder will be available for autocompletion and execution
within your user account.

## Fuzzy lookup

If you're feeling really lazy, fuzzy lookup can come to your aid. If you don't quite feel
like typing `tools git:clone ...`, you're welcome to type `tools g:c ...`. As long as your
arguments are specific enough to target one script, you're good to go.