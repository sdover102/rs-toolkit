#!/usr/bin/ruby

CURR_DIR = Dir.pwd
LIB_DIR = 'lib'
USER_LIB_DIR = File.expand_path('~') + '/.tools'

def get_completion_list
    completions = []

    dirs = Dir.glob(File.join(File.dirname(__FILE__), "#{LIB_DIR}/*"))
        .concat(Dir.glob(File.join(USER_LIB_DIR, '*')))
    dirs.each do |dir|
        base_string = File.basename(dir)
        files = Dir.glob(File.join(dir, "*"))
        files.each do |file|
            next if file.index('.txt') != nil
            completions.push "#{base_string}:" + File.basename(file, File.extname(file))
        end
    end

    completions.sort()
end

def show_help_message
    tools_script_name = 'tools'
    gen_err_message = "Usage: #{tools_script_name} [directory]:[script] <arg1> <arg2> <...>"
    puts gen_err_message
    puts "-" * 60
    puts "#{tools_script_name} is a utility to help "\
        "you organize and run scripts in \na generic way. "\
        "Here are a list of potential commands"

    completions = get_completion_list()
    completions.each do |completion|
        puts " - #{tools_script_name} #{completion}"
    end
    puts "-" * 60
    abort()
end

def load_help_file(full_path)
    txt_path = "#{full_path}.txt"
    if not File.exist?(txt_path)
        puts "Readme file doesn't exist at #{txt_path}"
        abort()
    end
    puts File.open(txt_path, 'r').read
    abort()
end

# Make sure we've got arguments
show_help_message() if ARGV.length == 0

arguments = ARGV

# Get class_method string
class_method = arguments.shift()

# Display help if requested
if class_method == '--help'
    show_help_message()
end

# Display list for zsh / bash autocompletion
if class_method == '--autocomplete-list'
    completions = get_completion_list
    puts completions.join(' ')
    abort()
end

# Make sure we have arguments in class:method form
show_help_message() if !class_method.index(':')

# Split this into an array to invoke method
class_method = class_method.split(':')

# Grab the directory / script
directory = class_method[0]
script = class_method[1]

# Lookup directory name
dirPath = Dir.glob(File.join(File.dirname(__FILE__), "#{LIB_DIR}/#{directory}*"))
    .concat(Dir.glob(File.join(USER_LIB_DIR, "#{directory}*")))

if dirPath.length > 1
    puts "Please be more specific. The following directories match your search:"
    dirPath.each do |path|
        puts " - #{path}"
    end
    abort()
end
directory = dirPath[0]

# Lookup file name
scriptPath = []
Dir.glob(File.join(directory, "#{script}*")).each do |file|
    next if file.index('.txt') != nil
    scriptPath.push(file)
end

if scriptPath.length > 1
    puts "Please be more specific. We found multiple matches in '#{directory}':"
    scriptPath.each do |path|
        puts " - #{path}"
    end
    abort()
end
fullPath = scriptPath[0]

# Make sure script is runnable
File.chmod(0755, fullPath)

# Run script
if arguments.length > 0
    joinedArgs = arguments.join('" "')
    if joinedArgs == '--help'
        load_help_file(fullPath)
    end
    system("#{fullPath} \"#{joinedArgs}\"")
else
    system(fullPath)
end

