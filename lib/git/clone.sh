#!/bin/bash

repoName=$1
if [ ! -z "$2" ]
    then
        repoName=$2
fi

git clone git@bitbucket.org:researchsquare/$1.git $repoName