#!/usr/bin/env php
<?php

$patch = $_SERVER['argv'][1];
$cacheFile = dirname(__FILE__) . '/patch-cache.txt';

if (file_exists($cacheFile)) {
    $cache = json_decode(file_get_contents($cacheFile), true);
}

if (!$patch) {
    echo "No patch specified";
    exit;
}

function arcCall($args, $endpoint) {
    $arc = 'arc call-conduit --conduit-uri="https://phabricator.sqr.io"';
    $args = json_encode($args);
    return json_decode(shell_exec('echo \''.$args.'\' | ' . $arc . ' ' . $endpoint), true);
}

// Get PHID
$phidInfo = arcCall(['names' => [$patch]], 'phid.lookup');
$phid = $phidInfo['response'][$patch]['phid'];

// Lookup query
$diffInfo = arcCall(['phids' => [$phid]], 'differential.query');

$repoId = $diffInfo['response'][0]['repositoryPHID'];

if (isset($cache[$repoId])) {
    $repoUri = $cache[$repoId];
} else {
    $repoInfo = arcCall(['phids' => [$repoId]], 'repository.query');
    $repoUri = $repoInfo['response'][0]['remoteURI'];
    $cache[$repoId] = $repoUri;
}

file_put_contents($cacheFile, json_encode($cache));

shell_exec("
    git clone $repoUri $patch &&
    cd $patch &&
    composer.phar install &&
    arc patch $patch &&
    ant init
");

?>