#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../" && pwd )

echo -e "Copy the following to your zsh profile\n*****"

echo "
tools() {
    ruby ${DIR}/tools.rb \$@
}
_rstoolkit_completion() {
    local a
    reply=(\`ruby ${DIR}/tools.rb --autocomplete-list\`)
}
compctl -K _rstoolkit_completion tools
"